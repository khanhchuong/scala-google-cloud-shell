FROM gcr.io/cloudshell-images/cloudshell:latest

# install scala
RUN sudo apt-get -y install scala

# install sbt
RUN echo "deb https://dl.bintray.com/sbt/debian /" | sudo tee -a /etc/apt/sources.list.d/sbt.list
RUN sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823
RUN sudo apt-get -y update
RUN sudo apt-get -y install sbt
