# scala-google-cloud-shell
[![pipeline status](https://gitlab.com/khanhchuong/scala-google-cloud-shell/badges/master/pipeline.svg)](https://gitlab.com/khanhchuong/scala-google-cloud-shell/commits/master)

## Objective
I like scala, google cloud shell and gitlab-ci. So I've made this small project to build a custom image by using gitlab-ci. It supports scala/sbt in google cloud shell.  

The custom image is public at:
```
gcr.io/chuongdk-public/scala-google-cloud-shell
```

## How to use
Modify *image URL* of cloud shell to the custom image URL (cf: Section *Custom Cloud Shell environment* in https://cloud.google.com/shell/docs/features)


![demo](demo.png)


